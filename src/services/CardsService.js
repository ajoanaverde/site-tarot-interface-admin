import Api from '@/services/Api'

class CardsService {
    // READ
    fetchCards() {
        return Api().get('cards')
    }
    // READ ONE
    fetchCard(id) {
        return Api().get('cards/' + id)
    }
    // CREATE
    newCard(data) {
        return Api().post('cards', data)
    }
    // UPDATE
    updateCard(id, data) {
        console.log(Api().put(`cards/${id}`, data));
        return Api().put('cards/' + id, data);
    }
    // DELETE
    deleteCard(id) {
        return Api().delete(`cards/${id}`)
    }

}

export default new CardsService();