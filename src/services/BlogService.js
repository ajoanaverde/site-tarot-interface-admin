import Api from '@/services/Api'

class BlogService {
    // READ
    fetchPosts() {
        return Api().get('posts')
    }
    // READ ONE
    fetchPost(id) {
        return Api().get('posts/' + id)
    }
    // CREATE
    newPost(data) {
        return Api().post('posts', data)
    }
    // UPDATE
    updatePost(id, data) {
        return Api().put('posts/' + id, data)
    }
    // DELETE
    deletePost(id) {
        return Api().delete(`posts/${id}`)
    }
    //
}

export default new BlogService();