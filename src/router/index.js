//
import Vue from "vue";
import VueRouter from "vue-router";
//
// import home page
import Blog from "../views/Blog"
import SinglePost from "../views/SinglePost"
import NewPost from "../views/NewPost"
import UpdatePost from "../views/UpdatePost"
import Cards from "../views/Cards"
import SingleCard from "../views/SingleCard"
import NewCard from "../views/NewCard"
import NotFound from "../views/NotFound"
//
Vue.use(VueRouter);
// je peux nester des routes dans un parent "admin"
const routes = [
    // {
    //     path: "/",
    //     name: "Home",
    //     component: 
    // },
    {
        path: "/blog",
        alias: "/",
        name: "Blog",
        component: Blog
    },
    {
        path: "/posts/:id",
        name: "Post",
        component: SinglePost
    },
    {
        path: "/newpost",
        name: "New Post",
        component: NewPost
    },
    {
        path: "/updatepost/:id",
        name: "Update Post",
        component: UpdatePost
    },
    {
        path: "/cards",
        name: "All Cards",
        component: Cards
    },
    {
        path: "/updatecard/:id",
        name: "Update Card",
        component: SingleCard
    },
    {
        path: "/newcard",
        name: "New Card",
        component: NewCard
    },
    {
        path: "*",
        name: "Not Found",
        component: NotFound
    }

];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes
});

export default router;
