import Vue from 'vue'
import App from './App.vue'
//
import router from "./router"
import VueRouter from "vue-router"
//requetes http:
import VueResource from "vue-resource"
//
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
//
Vue.config.productionTip = false
Vue.use(BootstrapVue, VueRouter, VueResource)

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
